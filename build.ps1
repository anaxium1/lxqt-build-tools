# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location
$outputpath = "$toppath\output"

mkdir qt
cd qt
$qtpath = Get-Location
curl.exe -L -o qt.zip https://gitlab.com/anaxium1/qt5/-/jobs/artifacts/master/raw/qt.zip?job=job
7z x qt.zip
rm qt.zip
cd $toppath

git clone https://github.com/kghost/lxqt-build-tools.git
cd lxqt-build-tools
mkdir build
cd build
cmake "-DCMAKE_INSTALL_PREFIX=$outputpath" "-DQt5Core_DIR=$qtpath\lib\cmake\Qt5Core" -G "NMake Makefiles" ..
nmake
nmake install

cd $outputpath
7z a -r ..\lxqt-build-tools.zip *
